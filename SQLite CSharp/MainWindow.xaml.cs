﻿using System.Windows;
using System.Data.SQLite;
using System;
using System.Data;
using Microsoft.Win32;
using System.Data.OleDb;
using System.IO;

namespace SQLite_CSharp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string databaseName;
        string tableName = "aip_tbl";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void updateBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SQLiteConnection conn = new SQLiteConnection("Data Source=database.db;Version=3;New=True;Compress=True;");
                conn.Open();
                SQLiteCommand command = conn.CreateCommand();

                command.CommandText = "update aip_tbl set ID='"+this.idTB.Text+"', GroupFolder='"+this.groupFolderTB.Text+"', Folder='"+this.folderTB.Text+"', Rachel='"+this.rachelTB.Text+"', Normalized='"+this.normalizedTB.Text+"' where ID='"+this.idTB.Text+"';";
                command.ExecuteNonQuery();
                MessageBox.Show("Updated");
                updateDataGrid();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void deleteBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SQLiteConnection conn = new SQLiteConnection("Data Source=database.db;Version=3;New=True;Compress=True;");
                conn.Open();
                SQLiteCommand command = conn.CreateCommand();

                command.CommandText = "delete from aip_tbl where ID='" + this.idTB.Text + "'";
                SQLiteDataReader reader = command.ExecuteReader();
                MessageBox.Show("Deleted");
                updateDataGrid();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void saveBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SQLiteConnection conn = new SQLiteConnection("Data Source=database.db;Version=3;New=True;Compress=True;");
                conn.Open();
                SQLiteCommand command = conn.CreateCommand();

                command.CommandText = "insert into aip_tbl (ID, GroupFolder, Folder, Rachel, Normalized) values ('" + this.idTB.Text + "','" + this.groupFolderTB.Text + "', '" + this.folderTB.Text + "', '" + this.rachelTB.Text + "', '"+this.normalizedTB.Text+"')";
                command.ExecuteNonQuery();
                MessageBox.Show("Saved");
                updateDataGrid();
                conn.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fetchBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SQLiteConnection conn = new SQLiteConnection("Data Source=database.db;Version=3;New=True;Compress=True;");
                conn.Open();
                SQLiteCommand command = conn.CreateCommand();

                command.CommandText = "select * from aip_tbl where ID='"+this.idTB.Text+"'";
                SQLiteDataReader reader = command.ExecuteReader();

                if(reader.Read())
                {
                    MessageBox.Show(reader["GroupFolder"].ToString());
                    MessageBox.Show(reader["Folder"].ToString());
                }
                else
                {
                    MessageBox.Show("Doesn't exist");
                }
                
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message+" Couldn't fetch");
            }
        }

        private void loadTableBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SQLiteConnection conn = new SQLiteConnection("Data Source=database.db;Version=3;New=True;Compress=True;");
                conn.Open();
                SQLiteCommand command = conn.CreateCommand();

                command.CommandText = "select * from aip_tbl;";
                command.ExecuteNonQuery();

                SQLiteDataAdapter dataAdater = new SQLiteDataAdapter(command);
                DataTable dataTable = new DataTable("aipTBL");
                dataAdater.Fill(dataTable);
                dataGrid.ItemsSource = dataTable.DefaultView;

                dataAdater.Update(dataTable);
                conn.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message+"Could not update data grid");
            }
        }

        private void updateDataGrid()
        {
            try
            {
                SQLiteConnection conn = new SQLiteConnection("Data Source=database.db;Version=3;New=True;Compress=True;");
                conn.Open();
                SQLiteCommand command = conn.CreateCommand();

                command.CommandText = "select * from aip_tbl;";
                command.ExecuteNonQuery();

                SQLiteDataAdapter dataAdater = new SQLiteDataAdapter(command);
                DataTable dataTable = new DataTable("aipTBL");
                dataAdater.Fill(dataTable);
                dataGrid.ItemsSource = dataTable.DefaultView;

                dataAdater.Update(dataTable);
                conn.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void clearFieldsBTN_Click(object sender, RoutedEventArgs e)
        {
            idTB.Text = String.Empty;
            groupFolderTB.Text = String.Empty;
            folderTB.Text = String.Empty;
            rachelTB.Text = String.Empty;
            normalizedTB.Text = String.Empty;
        }

        private void selectEMPBTN_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFD = new OpenFileDialog();
            openFD.Filter = "Excel File (*.xlsx)|*.xlsx";
            if(openFD.ShowDialog() == true)
            {
                empReportTB.Text = openFD.FileName;
            }
        }

        private void readEMPBTN_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrWhiteSpace(this.empReportTB.Text) || string.IsNullOrWhiteSpace(this.sheetName.Text))
            {
                MessageBox.Show("Please select an AIP Excel File with a valid sheet name.");
            }
            else
            {
                turnExcelToDB();
            }
        }

        private void turnExcelToDB()
        {
            try
            {
                OleDbConnection conn;
                DataSet dataSet = new DataSet();
                OleDbDataAdapter dataAdapter;
                conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.empReportTB.Text + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                dataAdapter = new OleDbDataAdapter("select * from ["+this.sheetName.Text+"$]", conn);
                dataAdapter.TableMappings.Add("Table", "aip_tbl");
                dataAdapter.Fill(dataSet);
                
                //
                dataGrid.ItemsSource = dataSet.Tables[0].DefaultView;

                //Check if db by excel file name exists or not
                if (checkDBExistsOrNot())
                {
                    //CREATING AIP TABLE 

                    //dropTable("database.db", "employees");
                    //createTable("database.db", "aip_tbl");
                    //MessageBox.Show("Created AIP table in db.");

                    updateDataGrid();

                }
                conn.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message+" Couldn't turn Excel to DB data. ");
            }
        }

        private bool checkDBExistsOrNot()
        {
            bool isExist = false;
            try
            {
                string[] files = Directory.GetFiles("../../bin/Debug", "*.db");
                foreach (string file in files)
                {
                    if (Path.GetFileName(file) == "database.db")
                    {
                        MessageBox.Show("Found Database: "+Path.GetFileName(file));
                        databaseName = Path.GetFileName(file);
                        if(checkIfDBTableExistsOrNot(databaseName))
                        {
                            //dropTable(databaseName);
                            //createTable(databaseName, tableName);
                            isExist = true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + " Something wrong with your directory. ");
            }

            return isExist;
        }


        private bool checkIfDBTableExistsOrNot(string databaseName)
        {
            bool found = false;
            try
            {
                SQLiteConnection conn = new SQLiteConnection("Data Source="+databaseName+";Version=3;New=True;Compress=True;");
                conn.Open();
                SQLiteCommand command = conn.CreateCommand();

                //command.CommandText = "select * from '"+tableName+"';";
                //createTable(databaseName, tableName);
                //command.ExecuteNonQuery();
                found = true;
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "Table with name aip_tbl not found");
            }
            return found;
        }

        private void dropTable(string databaseName, string tableName)
        {
            try
            {
                SQLiteConnection conn = new SQLiteConnection("Data Source=" + databaseName + ";Version=3;New=True;Compress=True;");
                conn.Open();
                SQLiteCommand command = conn.CreateCommand();
                command.CommandText = "drop table '" + tableName + "';";
                command.ExecuteNonQuery();
                conn.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + ". Could not drop table " + tableName);
            }
        }

        private void createTable(string databaseName, string tableName)
        {
            try
            {
                SQLiteConnection conn = new SQLiteConnection("Data Source=" + databaseName + ";Version=3;New=True;Compress=True;");
                conn.Open();
                SQLiteCommand command = conn.CreateCommand();
                command.CommandText = "create table '" + tableName + "' (id, group_folder, folder, rachel, normalized); ";
                command.ExecuteNonQuery();
                conn.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + "Could not create table.");
            }
        }

    }
}
